//
//  ListViewController.swift
//  ios-app
//
//  Created by Moch Ilham Fathoni on 12/03/20.
//  Copyright © 2020 Ceramic Pro. All rights reserved.
//

import UIKit
import Toast_Swift

class ListViewController: UIViewController {

    private let client = ClientService()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var search: UISearchBar!
    
    var filteredDataSource: [DataPesanan] = []
    var dataSource: [DataPesanan] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "STATUS ORDER"
        tableView.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(OrderViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        // edit ui table view
//        self.tableView.tableFooterView = UIView()
//        self.tableView.backgroundColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadDataSource()
        search.becomeFirstResponder()
    }

    func loadDataSource() {
        let id: Int? = UserDefaults.standard.integer(forKey: "id_cust")
        client.getPesanan(id: id!) { (json, error) in
            if let error = error {
                self.view.makeToast(error.localizedDescription)
            } else if let json = json {
                if json.error == false {
                    self.dataSource = json.data
                    self.filteredDataSource = self.dataSource
                    self.tableView.reloadData()
                } else {
                    self.view.makeToast(json.message)
                }
            }
        }
    }

    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tableViewTopConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.tableView.isHidden = false
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            filteredDataSource = dataSource
            tableView.reloadData()
            return
        }
        filteredDataSource = dataSource.filter({ data -> Bool in
            if data.merek!.lowercased().contains(searchText.lowercased()) {
                return data.merek!.lowercased().contains(searchText.lowercased())
            }
            if data.plat!.lowercased().contains(searchText.lowercased()) {
                return data.plat!.lowercased().contains(searchText.lowercased())
            }
            if data.paket!.lowercased().contains(searchText.lowercased()) {
                return data.paket!.lowercased().contains(searchText.lowercased())
            }
            return data.status!.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}

extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellContent", for: indexPath) as? OrderTableViewCell else {
            return UITableViewCell()
        }
        
        cell.viewCell.tableStyle(color: UIColor(red: CGFloat(234.0/255.0), green: CGFloat(234.0/255.0), blue: CGFloat(234.0/255.0), alpha: CGFloat(1.0)), borderColor: UIColor.lightGray.cgColor, width: 1.0)
        
        let id: Int = filteredDataSource[indexPath.row].id!
        cell.txtId.text = "   " + "Data Order #" + "\(id)"
        cell.lblStatus.text = "Status" + "        "
        cell.txtStatus.text = (filteredDataSource[indexPath.row].status ?? "") + "   "
        cell.txtTipe.text = filteredDataSource[indexPath.row].tipe
//        cell.txtTipe.backgroundColor = .white
        cell.txtWarna.text = filteredDataSource[indexPath.row].warna
//        cell.txtWarna.backgroundColor = .white
        cell.txtMerek.text = filteredDataSource[indexPath.row].merek
//        cell.txtMerek.backgroundColor = .white
        cell.txtPlat.text = filteredDataSource[indexPath.row].plat
//        cell.txtPlat.backgroundColor = .white
        cell.txtKondisi.text = filteredDataSource[indexPath.row].kondisi
//        cell.txtKondisi.backgroundColor = .white
        cell.txtRangka.text = filteredDataSource[indexPath.row].rangka
//        cell.txtRangka.backgroundColor = .white
        cell.txtTahun.text = filteredDataSource[indexPath.row].tahun
//        cell.txtTahun.backgroundColor = .white
        cell.txtPaket.text = filteredDataSource[indexPath.row].paket
//        cell.txtPaket.backgroundColor = .white
        cell.txtMasuk.text = filteredDataSource[indexPath.row].masuk
//        cell.txtMasuk.backgroundColor = .white
        cell.txtKeluar.text = filteredDataSource[indexPath.row].keluar
//        cell.txtKeluar.backgroundColor = .white
        cell.txtPJ.text = filteredDataSource[indexPath.row].pj
//        cell.txtPJ.backgroundColor = .white
        cell.txtGaransi.text = filteredDataSource[indexPath.row].garansi
//        cell.txtGaransi.backgroundColor = .white
        cell.txtKeterangan.text = filteredDataSource[indexPath.row].keterangan
//        cell.txtKeterangan.backgroundColor = .white
        cell.txtAntar.text = filteredDataSource[indexPath.row].antar
//        cell.txtAntar.backgroundColor = .white
        cell.txtBiaya.text = filteredDataSource[indexPath.row].biaya
//        cell.txtBiaya.backgroundColor = .white
        
        if indexPath.row % 2 == 0 {
            cell.headerCell.addBackground(color: UIColor(red: CGFloat(20.0/255.0), green: CGFloat(40.0/255.0), blue: CGFloat(80.0/255.0), alpha: CGFloat(1.0)))
        } else {
            cell.headerCell.addBackground(color: UIColor(red: CGFloat(12.0/255.0), green: CGFloat(123.0/255.0), blue: CGFloat(147.0/255.0), alpha: CGFloat(1.0)))
        }

        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        search.endEditing(true)
    }
}

//
//  OrderTableViewCell.swift
//  ios-app
//
//  Created by Moch Ilham Fathoni on 12/03/20.
//  Copyright © 2020 Ceramic Pro. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var txtId: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var txtStatus: UILabel!
    @IBOutlet weak var txtTipe: UITextView!
    @IBOutlet weak var txtWarna: UITextView!
    @IBOutlet weak var txtMerek: UITextView!
    @IBOutlet weak var txtPlat: UITextView!
    @IBOutlet weak var txtKondisi: UITextView!
    @IBOutlet weak var txtRangka: UITextView!
    @IBOutlet weak var txtTahun: UITextView!
    @IBOutlet weak var txtPaket: UITextView!
    @IBOutlet weak var txtMasuk: UITextView!
    @IBOutlet weak var txtKeluar: UITextView!
    @IBOutlet weak var txtPJ: UITextView!
    @IBOutlet weak var txtGaransi: UITextView!
    @IBOutlet weak var txtKeterangan: UITextView!
    @IBOutlet weak var txtAntar: UITextView!
    @IBOutlet weak var txtBiaya: UITextView!
    @IBOutlet weak var headerCell: UIStackView!
    @IBOutlet weak var viewCell: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
    func tableStyle(color: UIColor, borderColor: CGColor, width: CGFloat) {
        let subView = UIView(frame: bounds)
        subView.layer.borderColor = borderColor
        subView.layer.borderWidth = width
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}

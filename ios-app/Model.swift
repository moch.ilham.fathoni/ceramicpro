//
//  Paket.swift
//  ios-app
//
//  Created by Moch Ilham Fathoni on 09/03/20.
//  Copyright © 2020 Ceramic Pro. All rights reserved.
//

import Foundation

struct Paket: Decodable {
    let error: Bool
    let message: String
    let data: [DataPaket]
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case message = "message"
        case data = "data"
    }
}

struct DataPaket: Decodable {
    let id_paket: Int
    let nama_paket: String
    
    enum CodingKeys: String, CodingKey {
        case id_paket = "id_paket"
        case nama_paket = "nama_paket"
    }
}

struct Franchise: Decodable {
    let error: Bool
    let message: String
    let data: [DataFranchise]
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case message = "message"
        case data = "data"
    }
}

struct DataFranchise: Decodable {
    let id_partner: Int
    let nama_bengkel: String
    
    enum CodingKeys: String, CodingKey {
        case id_partner = "id_partner"
        case nama_bengkel = "nama_bengkel"
    }
}

struct Tipe: Decodable {
    let error: Bool
    let message: String
    let data: [DataTipe]
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case message = "message"
        case data = "data"
    }
}

struct DataTipe: Decodable {
    let id: Int
    let tipe_kendaraan: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case tipe_kendaraan = "tipe_kendaraan"
    }
}

struct Profile: Decodable {
    let error: Bool
    let message: String
    let data: DataProfile
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case message = "message"
        case data = "data"
    }
}

struct DataProfile: Decodable {
    let id_cust: Int?
    let nama: String?
    let alamat: String?
    let no_hp: String?
    let email: String?
    
    enum CodingKeys: String, CodingKey {
        case id_cust = "id_cust"
        case nama = "nama"
        case alamat = "alamat"
        case no_hp = "no_hp"
        case email = "email"
    }
}

struct Login: Decodable {
    let error: Bool
    let message: String
    let data: DataUser
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case message = "message"
        case data = "data"
    }
}

struct DataUser: Decodable {
    let id_user: Int?
    let nama: String?
    let username: String?
    let email: String?
    let kategori: Int?
    let id_cust: Int?
    let id_partner: Int?
    
    enum CodingKeys: String, CodingKey {
        case id_user = "id_user"
        case nama = "nama"
        case username = "username"
        case email = "email"
        case kategori = "kategori"
        case id_cust = "id_cust"
        case id_partner = "id_partner"
    }
}

struct Order: Decodable {
    let error: Bool
    let message: String

    enum CodingKeys: String, CodingKey {
        case error = "error"
        case message = "message"
    }
}

struct Pesanan: Decodable {
    let error: Bool
    let message: String
    let data: [DataPesanan]
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case message = "message"
        case data = "data"
    }
}

struct DataPesanan: Decodable {
    let id: Int?
    let status: String?
    let tipe: String?
    let warna: String?
    let merek: String?
    let plat: String?
    let kondisi: String?
    let rangka: String?
    let tahun: String?
    let paket: String?
    let masuk: String?
    let keluar: String?
    let pj: String?
    let garansi: String?
    let keterangan: String?
    let antar: String?
    let biaya: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case status = "status"
        case tipe = "tipe"
        case warna = "warna"
        case merek = "merek"
        case plat = "plat"
        case kondisi = "kondisi"
        case rangka = "rangka"
        case tahun = "tahun"
        case paket = "paket"
        case masuk = "masuk"
        case keluar = "keluar"
        case pj = "pj"
        case garansi = "garansi"
        case keterangan = "keterangan"
        case antar = "antar"
        case biaya = "biaya"
    }
}

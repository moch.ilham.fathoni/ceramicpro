//
//  OrderViewController.swift
//  ios-app
//
//  Created by Moch Ilham Fathoni on 11/03/20.
//  Copyright © 2020 Ceramic Pro. All rights reserved.
//

import UIKit
import Toast_Swift

class OrderViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var txtTipe: UITextField! {
       didSet {
            txtTipe.setIcon(UIImage(named: "icon-drop-down")!)
       }
    }
    @IBOutlet weak var txtMerek: UITextField!
    @IBOutlet weak var txtKondisi: UITextField! {
       didSet {
            txtKondisi.setIcon(UIImage(named: "icon-drop-down")!)
       }
    }
    @IBOutlet weak var txtTahun: UITextField!
    @IBOutlet weak var txtWarna: UITextField!
    @IBOutlet weak var txtPlat: UITextField!
    @IBOutlet weak var txtRangka: UITextField!
    @IBOutlet weak var txtAntar: UITextField! {
       didSet {
            txtAntar.setIcon(UIImage(named: "icon-drop-down")!)
       }
    }
    @IBOutlet weak var txtPaket: UITextField! {
       didSet {
            txtPaket.setIcon(UIImage(named: "icon-drop-down")!)
       }
    }
    @IBOutlet weak var tglMasuk: UITextField! {
       didSet {
            tglMasuk.setIcon(UIImage(named: "icon-drop-down")!)
       }
    }
    @IBOutlet weak var txtFranchise: UITextField! {
       didSet {
            txtFranchise.setIcon(UIImage(named: "icon-drop-down")!)
       }
    }
    @IBOutlet weak var txtKeterangan: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    private let client = ClientService()
    private var selectTipe: UIPickerView?
    private var selectKondisi: UIPickerView?
    private var selectAntar: UIPickerView?
    private var selectPaket: UIPickerView?
    private var datePicker: UIDatePicker?
    private var selectFranchise: UIPickerView?
    
    private var dataTipe = [DataTipe]()
    private let dataKondisi = ["Baru", "Bekas"]
    private let dataAntar = ["Diantar", "Dijemput"]
    private var dataPaket = [DataPaket]()
    private var dataFranchise = [DataFranchise]()
    private var id_tipe: String? = nil
    private var kondisi: String? = nil
    private var antar: String? = nil
    private var id_paket: String? = nil
    private var id_partner: String? = nil
    private var nama: String? = nil
    private var alamat: String? = nil
    private var no_hp: String? = nil
    private var email: String? = nil
    private let id_cust: String? = UserDefaults.standard.string(forKey: "id_cust")

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "FORM ORDER"
        selectTipe = UIPickerView()
        selectKondisi = UIPickerView()
        selectAntar = UIPickerView()
        selectPaket = UIPickerView()
        datePicker = UIDatePicker()
        selectFranchise = UIPickerView()
        
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(OrderViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(OrderViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        txtTipe.inputView = selectTipe
        txtKondisi.inputView = selectKondisi
        txtAntar.inputView = selectAntar
        txtPaket.inputView = selectPaket
        tglMasuk.inputView = datePicker
        txtFranchise.inputView = selectFranchise
        
        btnSubmit.layer.cornerRadius = 10.0
        btnSubmit.layer.borderWidth = 1
        btnSubmit.layer.borderColor = UIColor.systemTeal.cgColor
        self.scrollView.contentLayoutGuide.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true

        selectTipe?.dataSource = self
        selectTipe?.delegate = self
        selectPaket?.dataSource = self
        selectPaket?.delegate = self
        selectFranchise?.dataSource = self
        selectFranchise?.delegate = self
        selectKondisi?.dataSource = self
        selectKondisi?.delegate = self
        selectAntar?.dataSource = self
        selectAntar?.delegate = self
        scrollView?.delegate = self
        reloadData()
        
        // edit ui text field
//        txtMerek.layer.borderColor = UIColor.gray.cgColor
//        txtMerek.layer.borderWidth = 1.0
//        txtMerek.layer.cornerRadius = 5.0
//        txtTahun.layer.borderColor = UIColor.gray.cgColor
//        txtTahun.layer.borderWidth = 1.0
//        txtTahun.layer.cornerRadius = 5.0
//        txtWarna.layer.borderColor = UIColor.gray.cgColor
//        txtWarna.layer.borderWidth = 1.0
//        txtWarna.layer.cornerRadius = 5.0
//        txtPlat.layer.borderColor = UIColor.gray.cgColor
//        txtPlat.layer.borderWidth = 1.0
//        txtPlat.layer.cornerRadius = 5.0
//        txtRangka.layer.borderColor = UIColor.gray.cgColor
//        txtRangka.layer.borderWidth = 1.0
//        txtRangka.layer.cornerRadius = 5.0
//        txtKeterangan.layer.borderColor = UIColor.gray.cgColor
//        txtKeterangan.layer.borderWidth = 1.0
//        txtKeterangan.layer.cornerRadius = 5.0
        
        // ketika tombol return pada keyboard iphone diklik maka akan close keyboard
//        txtMerek.delegate = self
//        txtTahun.delegate = self
//        txtWarna.delegate = self
//        txtPlat.delegate = self
//        txtRangka.delegate = self
//        txtKeterangan.delegate = self
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        tglMasuk.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reset()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Untuk disable scroll
//        if scrollView.contentOffset.x != 0 {
//            scrollView.contentOffset.x = 0
//        }
    }

    @IBAction func submit(_ sender: UIButton) {
        let tipe: String = txtTipe.text!
        let merek: String = txtMerek.text!
        let tahun: String = txtTahun.text!
        let warna: String = txtWarna.text!
        let plat: String = txtPlat.text!
        let rangka: String = txtRangka.text!
        let keterangan: String = txtKeterangan.text!
        let tgl: String = tglMasuk.text!
        
        if tipe.isEmpty || id_tipe!.isEmpty {
            self.view.makeToast("Pilih tipe kendaraan!")
            return
        }
        if merek.isEmpty {
            self.view.makeToast("Merek mobil harus diisi!")
            return
        }
        if txtKondisi.text!.isEmpty || kondisi!.isEmpty {
            self.view.makeToast("Pilih kondisi mobil!")
            return
        }
        if tahun.isEmpty {
            self.view.makeToast("Tahun harus diisi!")
            return
        }
        if warna.isEmpty {
            self.view.makeToast("Warna mobil harus diisi!")
            return
        }
        if plat.isEmpty {
            self.view.makeToast("Nomor plat mobil harus diisi!")
            return
        }
        if rangka.isEmpty {
            self.view.makeToast("Nomor rangka mobil harus diisi!")
            return
        }
        if txtAntar.text!.isEmpty || antar!.isEmpty {
            self.view.makeToast("Pilih opsi pengantaran!")
            return
        }
        if txtPaket.text!.isEmpty || id_paket!.isEmpty {
            self.view.makeToast("Pilih paket!")
            return
        }
        if tgl.isEmpty {
            self.view.makeToast("Tanggal masuk harus diisi!")
            return
        }
        if txtFranchise.text!.isEmpty || id_partner!.isEmpty {
            self.view.makeToast("Pilih franchise!")
            return
        }
        if keterangan.isEmpty {
            self.view.makeToast("Keterangan harus diisi!")
            return
        }
        
        client.order(id_cust: id_cust!, nama: nama ?? " ", alamat: alamat ?? " ", no_hp: no_hp ?? " ", email: email ?? " ", tipe_kendaraan: id_tipe!, merk_kendaraan: merek, kondisi: kondisi!, tahun: tahun, warna: warna, no_plat: plat, no_rangka: rangka, pilihan_antar: antar!, id_paket: id_paket!, tgl_masuk: tgl, id_partner: id_partner!, keterangan: keterangan, sender: self)
        
        print(email ?? " ")
        print(no_hp ?? " ")
        print(alamat ?? " ")
        print(nama ?? " ")
        print(id_cust!)
        print(id_tipe!)
        print(merek)
        print(kondisi!)
        print(tahun)
        print(warna)
        print(plat)
        print(rangka)
        print(antar!)
        print(id_paket!)
        print(tgl)
        print(id_partner!)
        print(keterangan)
        reset()
    }

    func reset() {
        id_tipe = nil
        kondisi = nil
        antar = nil
        id_paket = nil
        id_partner = nil
        txtTipe.text = ""
        txtMerek.text = ""
        txtKondisi.text = ""
        txtTahun.text = ""
        txtWarna.text = ""
        txtPlat.text = ""
        txtRangka.text = ""
        txtAntar.text = ""
        txtPaket.text = ""
        txtFranchise.text = ""
        txtKeterangan.text = ""
        tglMasuk.text = ""
        
        reloadData()
        selectKondisi?.reloadAllComponents()
        selectKondisi?.selectRow(0, inComponent: 0, animated: true)
        selectAntar?.reloadAllComponents()
        selectAntar?.selectRow(0, inComponent: 0, animated: true)
    }
    
    func reloadData() {
        client.getTipe() { (json, error) in
            if let error = error {
                self.view.makeToast(error.localizedDescription)
            } else if let json = json {
                if json.error == false {
                    self.dataTipe = json.data
                } else {
                    self.view.makeToast(json.message)
                }
            }
            self.selectTipe?.reloadAllComponents()
            self.selectTipe?.selectRow(0, inComponent: 0, animated: true)
        }
        
        client.getPaket() { (json, error) in
            if let error = error {
                self.view.makeToast(error.localizedDescription)
            } else if let json = json {
                if json.error == false {
                    self.dataPaket = json.data
                } else {
                    self.view.makeToast(json.message)
                }
            }
            self.selectPaket?.reloadAllComponents()
            self.selectPaket?.selectRow(0, inComponent: 0, animated: true)
        }
        
        client.getFranchise() { (json, error) in
            if let error = error {
                self.view.makeToast(error.localizedDescription)
            } else if let json = json {
                if json.error == false {
                    self.dataFranchise = json.data
                } else {
                    self.view.makeToast(json.message)
                }
            }
            self.selectFranchise?.reloadAllComponents()
            self.selectFranchise?.selectRow(0, inComponent: 0, animated: true)
        }
        
        client.getProfile(id: Int(id_cust!)!) { (json, error) in
            if let error = error {
                self.view.makeToast(error.localizedDescription)
            } else if let json = json {
                if json.error == false {
                    self.nama = json.data.nama
                    self.alamat = json.data.alamat
                    self.no_hp = json.data.no_hp
                    self.email = json.data.email
                } else {
                    self.view.makeToast(json.message)
                }
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UITextField {
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
                      CGRect(x: 10, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
//        leftView = iconContainerView
//        leftViewMode = .always
        rightView = iconContainerView
        rightViewMode = .always
    }
}

extension OrderViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == selectTipe {
            return dataTipe.count
        } else if pickerView == selectKondisi{
            return dataKondisi.count
        } else if pickerView == selectAntar{
            return dataAntar.count
        } else if pickerView == selectPaket{
            return dataPaket.count
        } else if pickerView == selectFranchise{
            return dataFranchise.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == selectTipe {
            id_tipe = String(dataTipe[row].id)
            txtTipe.text = dataTipe[row].tipe_kendaraan
            txtTipe.resignFirstResponder()
        } else if pickerView == selectKondisi{
            kondisi = dataKondisi[row]
            txtKondisi.text = dataKondisi[row]
            txtKondisi.resignFirstResponder()
        } else if pickerView == selectAntar{
            antar = dataAntar[row]
            txtAntar.text = dataAntar[row]
            txtAntar.resignFirstResponder()
        } else if pickerView == selectPaket{
            id_paket = String(dataPaket[row].id_paket)
            txtPaket.text = dataPaket[row].nama_paket
            txtPaket.resignFirstResponder()
        } else if pickerView == selectFranchise{
            id_partner = String(dataFranchise[row].id_partner)
            txtFranchise.text = dataFranchise[row].nama_bengkel
            txtFranchise.resignFirstResponder()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == selectTipe {
            return dataTipe[row].tipe_kendaraan
        } else if pickerView == selectKondisi{
            return dataKondisi[row]
        } else if pickerView == selectAntar{
            return dataAntar[row]
        } else if pickerView == selectPaket{
            return dataPaket[row].nama_paket
        } else if pickerView == selectFranchise{
            return dataFranchise[row].nama_bengkel
        }
        return nil
    }
    
//    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//            var label = UILabel()
//            if let v = view {
//                label = v as! UILabel
//            }
//            label.font = UIFont (name: "Helvetica Neue", size: 15)
//            if pickerView == selectTipe {
//                label.text = dataTipe[row].tipe_kendaraan
//            } else if pickerView == selectKondisi{
//                label.text = dataKondisi[row]
//            } else if pickerView == selectAntar{
//                label.text = dataAntar[row]
//            } else if pickerView == selectPaket{
//                label.text = dataPaket[row].nama_paket
//            } else if pickerView == selectFranchise{
//                label.text = dataFranchise[row].nama_bengkel
//            }
//            label.textAlignment = .center
//
//            // edit ui label dan picker view
//            label.textColor = .black
//            pickerView.subviews[1].backgroundColor = .gray
//            pickerView.subviews[2].backgroundColor = .gray
//
//            return label
//    }
}

//
//  ViewController.swift
//  ios-app
//
//  Created by Moch Ilham Fathoni on 07/03/20.
//  Copyright © 2020 Ceramic Pro. All rights reserved.
//

import UIKit
import Toast_Swift

class LoginViewController: UIViewController, UITextFieldDelegate {

    private let client = ClientService()
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btnLogin.layer.cornerRadius = 10.0
        btnLogin.layer.borderWidth = 1
        btnLogin.layer.borderColor = UIColor.systemTeal.cgColor
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(OrderViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        // edit ui text field
//        txtUsername.layer.borderColor = UIColor.gray.cgColor
//        txtPassword.layer.borderColor = UIColor.gray.cgColor
//        txtUsername.layer.borderWidth = 1.0
//        txtPassword.layer.borderWidth = 1.0
//        txtUsername.layer.cornerRadius = 10.0
//        txtPassword.layer.cornerRadius = 10.0
//        txtUsername.delegate = self
//        txtPassword.delegate = self
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        btnLogin.isEnabled = true
    }

    // Set the shouldAutorotate to False
    override open var shouldAutorotate: Bool {
       return false
    }

    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
       return .portrait
    }

    @IBAction func Login(_ sender: Any) {
        let username: String = txtUsername.text!
        let password: String = txtPassword.text!
        
        if username.isEmpty || password.isEmpty {
            self.view.makeToast("Username or Password is required!")
            return
        }
        client.login(username: username, password: password) { (json, error) in
            if let error = error {
                self.view.makeToast(error.localizedDescription)
            } else if let json = json {
                if json.error == false {
                    self.view.makeToast(json.message, duration: 1.0, position: .bottom)
                    UserDefaults.standard.set(json.data.id_cust, forKey: "id_cust")
                    UserDefaults.standard.set(json.data.id_partner, forKey: "id_partner")
                    UserDefaults.standard.set(json.data.kategori, forKey: "kategori")
//                    self.btnLogin.isEnabled = false
                    self.performSegue(withIdentifier: "Main", sender: self)
                } else {
                    self.view.makeToast(json.message)
                }
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
}


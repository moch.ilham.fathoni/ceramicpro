//
//  AccountViewController.swift
//  ios-app
//
//  Created by Moch Ilham Fathoni on 11/03/20.
//  Copyright © 2020 Ceramic Pro. All rights reserved.
//

import UIKit
import Toast_Swift

class AccountViewController: UIViewController {

    private let client = ClientService()
    @IBOutlet weak var txtNama: UILabel!
    @IBOutlet weak var txtAlamat: UITextView!
    @IBOutlet weak var txtNoHp: UILabel!
    @IBOutlet weak var txtEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "SETTING"
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    @IBAction func logout(_ sender: UIButton) {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)

        self.dismiss(animated: true, completion: nil)
    }
    
    func loadData() {
        let id: Int? = UserDefaults.standard.integer(forKey: "id_cust")
        client.getProfile(id: id!) { (json, error) in
            if let error = error {
                self.view.makeToast(error.localizedDescription)
            } else if let json = json {
                if json.error == false {
                    self.txtNama.text = json.data.nama
                    self.txtAlamat.text = json.data.alamat
                    self.txtNoHp.text = json.data.no_hp
                    self.txtEmail.text = json.data.email
                } else {
                    self.view.makeToast(json.message)
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

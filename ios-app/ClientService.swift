//
//  NetworkingClient.swift
//  ios-app
//
//  Created by Moch Ilham Fathoni on 09/03/20.
//  Copyright © 2020 Ceramic Pro. All rights reserved.
//

import Foundation
import Alamofire
import Toast_Swift

class ClientService {

    fileprivate var baseUrl = "https://ceramicpro.co.id/apps/api_mobile.php"
    typealias LoginResponse = (Login?, Error?) -> Void
    typealias ProfileResponse = (Profile?, Error?) -> Void
    typealias TipeResponse = (Tipe?, Error?) -> Void
    typealias FranchiseResponse = (Franchise?, Error?) -> Void
    typealias PaketResponse = (Paket?, Error?) -> Void
    typealias PesananResponse = (Pesanan?, Error?) -> Void

    func getPaket(completion: @escaping PaketResponse) {
        AF.request(self.baseUrl + "?action=paket", method: .get,
                   parameters: nil, encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response {
                    (responseData) in
                    switch responseData.result {
                    case .success:
                        guard let data = responseData.data else {return}
                        do {
                            let paket = try JSONDecoder().decode(Paket.self, from: data)
                            completion(paket, nil)
                        } catch {
                            completion(nil, error)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
        }
    }
    
    func getFranchise(completion: @escaping FranchiseResponse) {
        AF.request(self.baseUrl + "?action=lokasi", method: .get,
                   parameters: nil, encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response {
                    (responseData) in
                    switch responseData.result {
                    case .success:
                        guard let data = responseData.data else {return}
                        do {
                            let franchise = try JSONDecoder().decode(Franchise.self, from: data)
                            completion(franchise, nil)
                        } catch {
                            completion(nil, error)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
        }
    }

    func getTipe(completion: @escaping TipeResponse) {
        AF.request(self.baseUrl + "?action=tipe", method: .get,
                   parameters: nil, encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response {
                    (responseData) in
                    switch responseData.result {
                    case .success:
                        guard let data = responseData.data else {return}
                        do {
                            let tipe = try JSONDecoder().decode(Tipe.self, from: data)
                            completion(tipe, nil)
                        } catch {
                            completion(nil, error)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
        }
    }

    func getProfile(id: Int, completion: @escaping ProfileResponse) {
        let parameters = ["id": id]
        AF.request(self.baseUrl + "?action=profile", method: .post,
                   parameters: parameters, encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response {
                    (responseData) in
                    switch responseData.result {
                    case .success:
                        guard let data = responseData.data else {return}
                        do {
                            let profile = try JSONDecoder().decode(Profile.self, from: data)
                            completion(profile, nil)
                        } catch {
                            completion(nil, error)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
        }
    }
    
    func login(username: String, password: String, completion: @escaping LoginResponse) {
        let parameters = ["username": username, "password": password]
        AF.request(self.baseUrl + "?action=login", method: .post,
                   parameters: parameters, encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response {
                    (responseData) in
                    switch responseData.result {
                    case .success:
                        guard let data = responseData.data else {return}
                        do {
                            let login = try JSONDecoder().decode(Login.self, from: data)
                            completion(login, nil)
                        } catch {
                            completion(nil, error)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
        }
    }

    func order(id_cust: String, nama: String, alamat: String, no_hp: String, email: String, tipe_kendaraan: String, merk_kendaraan: String, kondisi: String, tahun: String, warna: String, no_plat: String, no_rangka: String, pilihan_antar: String, id_paket: String, tgl_masuk: String, id_partner: String, keterangan: String, sender: UIViewController) {
        let parameters = ["id_cust": id_cust, "nama": nama, "alamat": alamat, "no_hp": no_hp, "email": email, "tipe_kendaraan": tipe_kendaraan, "merk_kendaraan": merk_kendaraan, "kondisi": kondisi, "tahun": tahun, "warna": warna, "no_plat": no_plat, "no_rangka": no_rangka, "kategori_jasa": "Otomotif", "pilihan_antar": pilihan_antar, "id_paket": id_paket, "tgl_masuk": tgl_masuk, "id_partner": id_partner, "keterangan": keterangan, "status": "ORDER"]
        AF.request(self.baseUrl + "?action=order", method: .post,
                   parameters: parameters, encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response {
                    (responseData) in
                    guard let data = responseData.data else {return}
                    do {
                        let order = try JSONDecoder().decode(Order.self, from: data)
                        sender.view.makeToast(order.message)
                    } catch {
                        sender.view.makeToast(error.localizedDescription)
                    }
        }
    }
    
    func getPesanan(id: Int, completion: @escaping PesananResponse) {
        let parameters = ["id": id]
        AF.request(self.baseUrl + "?action=list", method: .post,
                   parameters: parameters, encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response {
                    (responseData) in
                    switch responseData.result {
                    case .success:
                        guard let data = responseData.data else {return}
                        do {
                            let list = try JSONDecoder().decode(Pesanan.self, from: data)
                            completion(list, nil)
                        } catch {
                            completion(nil, error)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
        }
    }

}
